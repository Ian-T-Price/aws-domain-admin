#!/usr/bin/env bash

### Helper functions to provide extra untilities and help

# --- help ---
function help_block  {
  echo -e "\n\tA script to assist updating contact details on ${fmt_green}${fmt_bold}AWS Registered Domains${fmt_end}\n"
  echo -e "\tAccepted CLI arguments are:\n"
  echo -e "\t[-h |--help]\t\t\t Prints this help"
  echo -e "\t[-i |--info]\t\t\t Publish an info block about the set-up."
  echo -e "\t[-v |--verbose]\t\t\t Verbose mode. Use -vv or -vvv for more\n"
  echo -e "\n\tAt least one arguement (-v will do) is required to run the program\n"
  
  # echo -e "\n\t[-m {n}] \t\t\t Number of MASTER nodes desired \t\tDefault = 1  NB: It needs to be an odd number"
  # echo -e "\t[-mc] \t\t\t\t CPUs of the MASTER Multipass VM(s) required \tDefault = 1"
  # echo -e "\t[-mm] \t\t\t\t Memory of the MASTER Multipass VM(s) required \tDefault = 1G"
  # echo -e "\t[-md] \t\t\t\t Disk of the MASTER Multipass VM(s) required \tDefault = 3G Min: 3G"
  # echo -e "\t[-mi | --master-image] \t\t Image of the MASTER Multipass VM(s) required \tDefault = release:20.04"
}

# --- set colors and formats for logs ---
function set_colours (){
  if [ "x$TERM" != "x" ] && [ "$TERM" != "dumb" ]; 
    then
    # Set foreground (text) colours
    set -a # export variables to the environment
    # fmt_black=$(tput setaf 0)
    fmt_red=$(tput setaf 1)
    fmt_green=$(tput setaf 2)
    fmt_yellow=$(tput setaf 3)
    #fmt_blue=$(tput setaf 4)
    fmt_purple=$(tput setaf 5)
    fmt_cyan=$(tput setaf 6)
    #fmt_white=$(tput setaf 7)
    fmt_bold=$(tput bold)
    #fmt_underline=$(tput sgr 0 1)
    fmt_end=$(tput sgr0)
    set +a
  fi
}

# --- helper functions for logs & info ---
function success {
  if (( VERBOSE_LEVEL >= 1 )) ; then echo -e "${fmt_green}${fmt_bold}[SUCCESS] ${fmt_end}" "$@" ; fi
}

function info {
  if (( VERBOSE_LEVEL >= 2 )) ; then echo -e "${fmt_yellow}[INFO]    ${fmt_end}" "$@" ; fi
}

function extra {
  if (( VERBOSE_LEVEL >= 3 )) ; then echo -e "${fmt_purple}[EXTRA-INFO] ${fmt_end}" "$@" ; fi
}

function warn {
  if (( VERBOSE_LEVEL >= 1 )) ; then echo -e "${fmt_cyan}${fmt_bold}[WARN]    ${fmt_end}" "$@" ; fi
}

function fatal {
  echo -e "${fmt_red}[ERROR] ${fmt_end}" "$@"
  echo -e "${fmt_red}[ERROR] ${fmt_end} Exiting with errors. Cleaning..."
  cleanup
}

# --- Change text case to lower
lowercase(){
    echo "$1" | sed "y/ABCDEFGHIJKLMNOPQRSTUVWXYZ/abcdefghijklmnopqrstuvwxyz/"
}
# --- Change text case to UPPER
uppercase(){
    echo "$1" | sed "y/abcdefghijklmnopqrstuvwxyz/ABCDEFGHIJKLMNOPQRSTUVWXYZ/"
}

# --- Trap CTRL+c to ensure no accidental quitting
function no_ctrlc()
{
    (( ctrlc_count++ ))
    echo
    if [[ $ctrlc_count == 1 ]]; then
        echo "Stop that."
    elif [[ $ctrlc_count == 2 ]]; then
        echo "Once more and I quit."
    else
        echo "That's it.  I quit."
        exit
    fi
}

# --- Check if a required variable is set 
function require_var {
  # Use it without the $, as in:
  #   require_env_var VARIABLE_NAME  
  # or   
  #   require_env_var VARIABLE_NAME "Some description of the variable"
  var_name="${1:-}"
    if [ -z "${!var_name:-}" ]; then
    info "The required variable ${var_name} is empty"
    if [ -n "${2:-}" ]; then
       info "  - $2"
    fi
    exit 1
  fi
}

# --- Check to see that we have a required binary on the path
function require_binary  {
  if [ -z "${1:-}" ]; then
    echo "${FUNCNAME[0]} requires an argument"
    exit 1
  fi  
  if ! [ -x "$(command -v "$1")" ]; then
    echo "The required executable '$1' is not on the path."
    exit 1
  fi
}

set_colours
# if [[ $# -eq 0 ]]; then help_block; exit 0; fi