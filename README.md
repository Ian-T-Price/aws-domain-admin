# Scripts to assist the updating of AWS registered domains

These scripts were written to automate the changing of addresses for multiple domains.  

I only have 12 domains registered on AWS at the moment but others will be moving over soon and I'm personally likely to be moving TWICE in the next 12 months.  Plus I'm a Senior DevOps Consultant who ought to be practicing what he preaches... ;-)

This requires a working `aws cli` with the credentials setup to enable the  `route53domains` command [(AWS documentation)](https://awscli.amazonaws.com/v2/documentation/api/latest/reference/route53domains/index.html#) to work

A good, quick test is `aws s3 ls` You should get a listing of all the buckets you have in AWS S3

Run the script with no parameters to get help:

```
$ clear; ./aws-domains-admin.bash 


	A script to assist updating contact details on AWS Registered Domains

	Accepted CLI arguments are:

	[-h |--help]			 Prints this help
	[-i |--info]			 Publish an info block about the set-up.
	[-v |--verbose]		         Verbose mode. Use -vv or -vvv for more


	At least one arguement (-v will do) is required to run the program
```
_**Using -vv for extra verbosity is highly recommended**_

```
$ clear; ./aws-domains-admin.bash -vv

[INFO]     Collecting domain details for carlton-carriages.com
[INFO]     Collecting domain details for carltoncarriages.com
[INFO]     Collecting domain details for centerline.biz
[INFO]     Collecting domain details for centreline.biz
[INFO]     Collecting domain details for community-conservation-tourism.com
[INFO]     Collecting domain details for horsedrawncarriage.company
[INFO]     Collecting domain details for horsedrawncarriage.org
[INFO]     Collecting domain details for horsesandcarriages.co.uk
[INFO]     Collecting domain details for horsesandcarriages.uk
[INFO]     Collecting domain details for iantprice.com
[INFO]     Collecting domain details for incredibleisabella.com
[INFO]     Collecting domain details for quesera.biz

	Do you wish to update the carlton-carriages.com domain or show the details?

1) Yes		  3) Show contacts  5) Quit
2) No		  4) show Privacy
   * Type the numeric option (Return to display options): 
```
The program first collects all the details about the registered domains and caches the info in a `json/domain-details` directory.

This is then used to create templates on a per group basis.  The `OrganizationName` field is used to group the templates.

The group templates live in the `json` directory. The proforma templates used to create these group templates are stored in `json/templetes`

**TODO Issue #6** Create generic templates and move them to a version controlled `templates` directory

You can edit the group files to apply update details for each `OrganizationName`

### Example contacts template: 
Name: `json/templates/template-update-domain-contact.json`

The `<<DOMAINNAME>>` tag should not be changed; this is modified by the program automatically.  

All `<<>>` tags need to be completed. `ExtraParams` key:value pairs can be removed if not required.

The code must be valid json; use [Code Beautify](https://codebeautify.org/) to validate
```
$ cat json/templates/template-update-domain-contact.json 
{
  "DomainName": "<<DOMAINNAME>>",
  "AdminContact": {
  ...
  },
  "TechContact": {
   ...
  },
  "RegistrantContact": {
    "FirstName": "<<FirstName>>",
    "LastName": "<<>>",
    "ContactType": "COMPANY",
    "OrganizationName": "<<>>",
    "AddressLine1": "<<>>",
    "AddressLine2": "<<>>",
    "City": "<<>>",
    "CountryCode": "<<>>",
    "ZipCode": "<<>>",
    "PhoneNumber": "<<>>",
    "Email": "<<>>",
    "ExtraParams": [
      {
        "Name": "UK_CONTACT_TYPE",
        "Value": "LTD"
      },
      {
        "Name": "UK_COMPANY_NUMBER",
        "Value": "<<>>"
      }
    ]
  }
}
```

### Example privacy template:
Name: `json/templates/template-update-domain-privacy.json`

The `<<DOMAINNAME>>` tag should not be changed; this is modified by the program automatically
```
{
  "DomainName": "<<DOMAINNAME>>",
  "AdminPrivacy": true,
  "RegistrantPrivacy": true,
  "TechPrivacy": true
}
```






