#!/usr/bin/env bash

  # aws cli breaks if REGION is double quoted...?
  # shellcheck disable=SC2086

### Set the variables
  ctrlc_count=0; trap no_ctrlc SIGINT
  DATE=$(date +"%Y-%m-%d_%H-%M")

  # This command, 'aws route53details' only runs in the us-east-1 Region. 
  REGION="--region us-east-1"

  # !!! Ensure these directories are excluded in'.gitignore' to ensure we don't publish our credentials on Git{Lab|Hub}
  # Get the details of all the domains so we have a backup to refer to - delete as necessary
  DETAILSDIR="json/domain-details"
  DOMAINDETAILS="$DETAILSDIR/$DATE""_domain-details.json"
  # Create the details of individual domains to use for the update - delete at any time - will be recreated
  UPDATESDIR="json/domain-updates"
  # Templates used to create INDIVIDUAL domain update files
  OUTPUTDIR="json"
  # The templates for non-existent GROUP templates (based on OrganizationName) - will need customising for each GROUP
  TEMPLATESDIR="json/templates"
  CONTACTSTEMPLATE="template-update-domain-contact.json"
  PRIVACYTEMPLATE="template-update-domain-privacy.json"

### Source the functions
  # shellcheck source=./functions/helpers.bash
  source ./functions/helpers.bash

# Get domain names
get_aws_domain_names() {
  aws route53domains list-domains $REGION | jq -r '.Domains[].DomainName'
}

# Get domain details 
get_aws_domain_details() {
  echo -e "{\n\"domains\": [" > "$DOMAINDETAILS"
  while read -r LINE
  do
      info "Collecting domain details for $LINE"
      aws route53domains $REGION get-domain-detail --domain-name "$LINE"  >> "$DOMAINDETAILS"
  done < <(get_aws_domain_names)
  ./domains-jsonfy.sed -i "$DOMAINDETAILS"
  echo -e "    ]\n}" >> "$DOMAINDETAILS"
}

get_unique_group_names() {
  # Use '.RegistrantContact.OrganizationName' to discern Domain Group automatically
  # JQ lines feel like a MVP - works but...
  if [[ $1 ]];
    then
      jq -r ".domains[] |  select( any(. ==\"$1\" )) | .RegistrantContact.OrganizationName | if type == \"string\" then split(\" \") | .[0] | ascii_downcase else . end"  < "$DOMAINDETAILS"
    else
      jq -r '.domains[].RegistrantContact.OrganizationName | if type == "string" then split(" ") | .[0] | ascii_downcase else . end' < "$DOMAINDETAILS" | sort | uniq | sed '/null/d'
    fi
}

# Group domain names
create_group_domain_contacts() {
  while read -r LINE;
    do
      CONTACTSFN="update-domain-contact-$LINE.json"
      PRIVACYFN="update-domain-privacy-$LINE.json"
      if [[ ! -f "$OUTPUTDIR/$CONTACTSFN" ]];
        then
          cp -f "$TEMPLATESDIR/$CONTACTSTEMPLATE" "$OUTPUTDIR/$CONTACTSFN"
          cp -f "$TEMPLATESDIR/$PRIVACYTEMPLATE" "$OUTPUTDIR/$PRIVACYFN" 
          warn "We are creating $OUTPUTDIR/$CONTACTSFN from the $TEMPLATESDIR/$CONTACTSTEMPLATE template."
          warn "You ${fmt_green}${fmt_bold}REALLY${fmt_end} ought to check the values to be applied before updating the domain."
          read -n 1 -s -r -p "Press any key to continue"
        fi
    done < <(get_unique_group_names)
}

update_aws_domain_contacts() {
  while read -r LINE <&3;
    do
      CONTACTSFN="update-domain-contact-$LINE.json"
      PRIVACYFN="update-domain-privacy-$LINE.json"
      ORGNAME=$(get_unique_group_names "$LINE")
      CONTACTSTEMPLATE="update-domain-contact-$ORGNAME.json"
      PRIVACYTEMPLATE="update-domain-privacy-$ORGNAME.json"
      
      cp -f "$OUTPUTDIR/$CONTACTSTEMPLATE" "$UPDATESDIR/$CONTACTSFN" 
      cp -f "$OUTPUTDIR/$PRIVACYTEMPLATE" "$UPDATESDIR/$PRIVACYFN" 
      sed -i "s/<<DOMAINNAME>>/$LINE/" "$UPDATESDIR/$CONTACTSFN"
      sed -i "s/<<DOMAINNAME>>/$LINE/" "$UPDATESDIR/$PRIVACYFN"
      
      echo -e "\n\t${fmt_green}${fmt_bold}Do you wish to update the ${fmt_red}${fmt_bold}$LINE${fmt_end}${fmt_green}${fmt_bold} domain or show the details?${fmt_end}\n"
      # TODO Add Diff option Issue #2 https://gitlab.com/Ian-T-Price/aws-domain-admin/-/issues/2
      export PS3="   * Type the numeric option (Return to display options): "
  select ynspq in "Yes" "No" "Show contacts" "show Privacy" "Quit";
    do
      case $ynspq in
            "Yes" ) aws route53domains update-domain-contact-privacy $REGION --cli-input-json file://"$UPDATESDIR/$PRIVACYFN"
                    aws route53domains update-domain-contact $REGION --cli-input-json file://"$UPDATESDIR/$CONTACTSFN"
                    # TODO Issue #3 https://gitlab.com/Ian-T-Price/aws-domain-admin/-/issues/3
                    REGEMAIL=$(jq -r '.RegistrantContact.Email' < "$UPDATESDIR/$CONTACTSFN")
                    success "$LINE contact details updated"
                    success "Nota Bene: There will be multiple emails sent to${fmt_red}${fmt_bold} $REGEMAIL${fmt_end}"
                    success "possibly including a validation email which needs to be confirmed.\n"
                    break
                    ;;
             "No" ) warn "${fmt_red}${fmt_bold}$LINE${fmt_end} was ${fmt_cyan}${fmt_bold}NOT${fmt_end} updated\n"
                    break
                    ;;
  "Show contacts" ) jq < "$UPDATESDIR/$CONTACTSFN"
                    ;;
   "show Privacy" ) jq < "$UPDATESDIR/$PRIVACYFN"
                    ;;
           "Quit" ) exit 0
                    ;;
      esac
    done
    done 3< <(get_aws_domain_names)
}

### Args ###
if [[ $# -eq 0 ]];
  then
  help_block; exit 0
  else
  set -u
  while [[ $# -gt 0 ]]; do
    case $1 in
      -h | '--help' ) k3s_help; exit 0 ;;
      -i | '--info' ) info_block; exit 0 ;;
      -v | '--verbose' ) VERBOSE_LEVEL=1 ;;
      -vv) VERBOSE_LEVEL=2 ;;
      -vvv) VERBOSE_LEVEL=3 ;;
      *) exit 1
      ;;
    esac
    shift
  done
  set +u
fi

############
### Main ###
############
get_aws_domain_details
create_group_domain_contacts
update_aws_domain_contacts