#!/usr/bin/env bash

# Reurn the details of the contact for all domains registered at AWS

### Source the functions
  # shellcheck source=./functions/helpers.bash
  source ./functions/helpers.bash

### Set the variables
  POSTFIXCMD=1
  SEDCMD=1
  FILEDIR="json/domain-details/"
  FILENAME="$(find $FILEDIR -print0 | xargs -r -0 ls -1 -t | head -1)"
  echo -e "\n${fmt_green}${fmt_bold}File name: $FILENAME ${fmt_end}\n"

# --- help ---
function help_block  {
  echo -e "\n\tProvides contact details for ${fmt_green}${fmt_bold}AWS Registered Domains${fmt_end}\n"
  echo -e "\tAccepted CLI arguments are:\n"
  echo -e "\t[-h |--help]\t Prints this help"
  echo -e "\n\t[-e |\t\t Output the Expiration details for all domains"
  echo -e "\n\t[-t |\t\t Output the Techinal Contact details values for all domains"
  echo -e "\t[-a |\t\t Output the Admin Contact details values for all domains"
  echo -e "\t[-r |\t\t Output the Registrant Contact details values for all domains"
  echo -e "\n\t[-rd |\t\t Output the Registrant Contact details as per -r but with key:values pairs"
  echo -e "\n\t[-tx |\t\t Output the Techinal Contact details for all domains with e(x)tra info"
  echo -e "\t[-ax |\t\t Output the Admin Contact details for all domains with e(x)tra info"
  echo -e "\t[-rx |\t\t Output the Registrant Contact details for all domains with e(x)tra info"
  #echo -e "\t[-v |--verbose]\t\t\t Verbose mode. Use -vv or -vvv for more\n"
  #echo -e "\n\tAt least one arguement (-v will do) is required to run the program"
  echo -e
}

function output-command() {
  # TODO Issue #4 https://gitlab.com/Ian-T-Price/aws-domain-admin/-/issues/4 
  if [[ $POSTFIXCMD == 1 ]];
    then 
       POSTFIX=("|" "column -t -s ','" )
      "${JQCOMMAND[@]}" < "$FILENAME" | column -t -s ','
    else
       POSTFIX=("|" 'sort' "|" 'jq -c')
      "${JQCOMMAND[@]}" < "$FILENAME" | sort | jq -c
    fi
  
  echo -e 

  if [[ $SEDCMD == 1 ]];
    # Second jq just adds the colour back in... ;-)
    then 
      echo -en "${JQCOMMAND[@]}" | sed "s/] ]/] ]'/g" | sed "s/jq -c /jq -c '/"
    else
      echo -en "${JQCOMMAND[@]}" | sed "s/}/}'/g" | sed "s/jq -c /jq -c '/"
    fi
  
  echo -e " < $FILENAME" "${POSTFIX[*]}" "\n"
}

############
### Main ###
############

### Args 
if [[ $# -eq 0 ]];
  then
  help_block; exit 0
  else
  set -u
  while [[ $# -gt 0 ]]; do
    case $1 in
      -h | '--help' ) help_block; exit 0 ;;
      -e  ) SEDCMD=2;
            POSTFIXCMD=2;
            JQCOMMAND=(jq -c '.domains[] | {Expires: .ExpirationDate, Domain: .DomainName}')
            ;;
      -t  ) SEDCMD=1;
            JQCOMMAND=(jq -c '.domains[] | [ .DomainName, .TechContact[] ]')
            ;;
      -a  ) SEDCMD=1;
            JQCOMMAND=(jq -c '.domains[] | [ .DomainName, .AdminContact[] ]')
            ;;
      -r  ) SEDCMD=1; 
            JQCOMMAND=(jq -c '.domains[] | [ .DomainName, .RegistrantContact[] ]')
            ;;
      -tx ) SEDCMD=2; 
            JQCOMMAND=(jq -c '.domains[].TechContact | {First: .FirstName, Last: .LastName, OrgType: .ContactType, OrgName: .OrganizationName,
                               Add1: .AddressLine1, City: .City, Country: .CountryCode, PostCode: .ZipCode, Tel: .PhoneNumber, Email: .Email}')
            ;;
      -ax ) SEDCMD=2; 
            JQCOMMAND=(jq -c '.domains[].AdminContact | {First: .FirstName, Last: .LastName, OrgType: .ContactType, OrgName: .OrganizationName,
                               Add1: .AddressLine1, City: .City, Country: .CountryCode, PostCode: .ZipCode, Tel: .PhoneNumber, Email: .Email}')
            ;;
      -rx ) SEDCMD=2;
            JQCOMMAND=(jq -c '.domains[].RegistrantContact | {First: .FirstName, Last: .LastName, OrgType: .ContactType, OrgName: .OrganizationName,
                               Add1: .AddressLine1, City: .City, Country: .CountryCode, PostCode: .ZipCode, Tel: .PhoneNumber, Email: .Email}')
            ;;
      -rd ) SEDCMD=2;
            POSTFIXCMD=2;
            JQCOMMAND=(jq -c '.domains[] | {Registrant: .RegistrantContact, Domain: .DomainName}')
            ;;
      # -v | '--verbose' ) VERBOSE_LEVEL=1 ;;
      # -vv) VERBOSE_LEVEL=2 ;;
      # -vvv) VERBOSE_LEVEL=3 ;;
      *) exit 1
      ;;
    esac
    shift
  done
  set +u
fi
output-command
