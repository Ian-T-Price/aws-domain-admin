#!/usr/bin/env bash

# Get the latest output of the domain details command and run it though jid -q 
# so we can easily build commands for jq. The result is piped directly into jq 

FILEDIR="json/domain-details/"
FILENAME="$(find $FILEDIR -print0 | xargs -r -0 ls -1 -t | head -1)"
echo -e "\nFile name: $FILENAME\n"

JQCOMMAND=$(jid -q < "$FILENAME" | tail -n 1)
jq "$JQCOMMAND" < "$FILENAME"

echo -e "\njq '$JQCOMMAND' < $FILENAME\n"
